package br.com.senac.cachoeira.model;



//Id , nome , informacoes , imagem e classificação

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Cachoeira implements Serializable {

    private int id   ;
    private String nome ;
    private String informocoes ;
    private  String imagem  ;
    private float classificacao ;

    public Cachoeira() {
    }

    public Cachoeira(int id, String nome, String informocoes, String imagem, float classificacao) {
        this.id = id;
        this.nome = nome;
        this.informocoes = informocoes;
        this.imagem = imagem;
        this.classificacao = classificacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInformocoes() {
        return informocoes;
    }

    public void setInformocoes(String informocoes) {
        this.informocoes = informocoes;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public float getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(float classificacao) {
        this.classificacao = classificacao;
    }

    @Override
    public String toString() {
        return this.nome + " "+ this.id;
    }


}
