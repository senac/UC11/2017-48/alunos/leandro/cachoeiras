package br.com.senac.cachoeira.view;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.EGLExt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;
import br.com.senac.cachoeira.DAO.CachoeiraDAO;

public class MainActivity extends AppCompatActivity {

    private CachoeiraDAO dao;

    public static final  int  REQUEST_NOVO = 1 ;

    private static Bitmap imagem   ;

    public static Bitmap getImagem(){
        return imagem;
    }

    private Cachoeira cachoeira;


    public static final String CACHOEIRA = "cachoeira" ;
    public static final String  IMAGEM =  "imagem" ;


    private ListView listView  ;
    private List<Cachoeira> lista  = new ArrayList<>() ;
    private ArrayAdapter<Cachoeira> adapter  ;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* inicializando cachoeiras padrao ..... */
      /*  Cachoeira rioDoMeio = new Cachoeira();
        rioDoMeio.setId(1);
        rioDoMeio.setNome(getResources().getString(R.string.Rio_do_Meio));
        rioDoMeio.setInformocoes(getResources().getString(R.string.Rio_do_Meio_info));
        rioDoMeio.setClassificacao(Float.parseFloat(getResources().getString(R.string.Rio_do_Meio_classificacao)));
        rioDoMeio.setImagem(BitmapFactory.decodeResource(getResources() , R.drawable.rio_do_meio));

        Cachoeira matilde = new Cachoeira();
        matilde.setId(2);
        matilde.setNome(getResources().getString(R.string.Matilde));
        matilde.setInformocoes(getResources().getString(R.string.Matilde_info));
        matilde.setClassificacao(Float.parseFloat(getResources().getString(R.string.Matilde_classificacao)));
        matilde.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.matilde));


        Cachoeira fumaca = new Cachoeira();
        fumaca.setId(3);
        fumaca.setNome(getResources().getString(R.string.Cachoeira_da_Fumaca));
        fumaca.setInformocoes(getResources().getString(R.string.Cachoeira_da_Fumaca_info));
        fumaca.setClassificacao(Float.parseFloat(getResources().getString(R.string.Cachoeira_da_Fumaca_classificacao)));
        fumaca.setImagem(BitmapFactory.decodeResource(getResources() , R.drawable.cachoeira_da_fumaca));

        lista.add(rioDoMeio);
        lista.add(matilde);
        lista.add(fumaca);*/


        listView = findViewById(R.id.ListaCachoeiras);

        registerForContextMenu(listView);

       listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
           @Override
           public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
               cachoeira = (Cachoeira) adapterView.getItemAtPosition(i);
               return false;
           }
       });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                Cachoeira cachoeira = (Cachoeira)adapter.getItemAtPosition(posicao) ;

                Intent intent = new Intent(MainActivity.this , DetalheActivity.class) ;
                intent.putExtra(CACHOEIRA , cachoeira);




                /* compactar a imagem para ser passada na intencao
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                cachoeira.getImagem().compress(Bitmap.CompressFormat.PNG , 100 , stream) ;
                byte[] bytes = stream.toByteArray() ;


                intent.putExtra(IMAGEM , bytes) ;

                */


                startActivity(intent);


            }
        });




    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuItem menuEditar = menu.add("Editar");
        menuEditar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(MainActivity.this,NovoActivity.class);
                intent.putExtra(CACHOEIRA, cachoeira);
                startActivity(intent);
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        listView = findViewById(R.id.ListaCachoeiras) ;

        dao = new CachoeiraDAO(this);
        lista = dao.getLista();
        dao.close();

        int layout = android.R.layout.simple_list_item_1;

        adapter = new ArrayAdapter<Cachoeira>(this, layout , lista) ;

        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;
    }

    public void novo(MenuItem item){

        Intent intent = new Intent(this , NovoActivity.class);
        startActivityForResult(intent , REQUEST_NOVO);


    }

/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_NOVO){

            switch (resultCode){
                case RESULT_OK :
                    Cachoeira cachoeira = (Cachoeira) data.getSerializableExtra(MainActivity.CACHOEIRA);
                    cachoeira.setImagem(NovoActivity.getImagem());
                    lista.add(cachoeira);
                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_CANCELED :
                        Toast.makeText(this, "Cancelou" , Toast.LENGTH_LONG).show();
                        break;

            }

        }



    }*/

    public void sobre (MenuItem item){
        Intent intent = new Intent(this , SobreActivity.class) ;
        startActivity(intent);


    }





















}
