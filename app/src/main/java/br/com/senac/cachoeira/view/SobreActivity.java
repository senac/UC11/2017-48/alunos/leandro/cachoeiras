package br.com.senac.cachoeira.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import br.com.senac.cachoeira.R;

public class SobreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);
    }
}
