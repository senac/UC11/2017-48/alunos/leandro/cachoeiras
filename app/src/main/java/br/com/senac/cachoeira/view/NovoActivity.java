package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.io.File;

import br.com.senac.cachoeira.DAO.CachoeiraDAO;
import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;
import br.com.senac.cachoeira.DAO.CachoeiraDAO;

public class NovoActivity extends AppCompatActivity {

    private static final int CAPTURA_IMAGEM = 1 ;
    private CachoeiraDAO dao;
    private Cachoeira cachoeiras;

    public static final  int  REQUEST_IMAGE_CAPTURE = 1 ;

    private static Bitmap imagem   ;

    public static Bitmap getImagem(){
        return imagem;
    }


    private ImageView imageView ;
    private EditText editTextNome ;
    private EditText editTextInformacoes ;
    private RatingBar ratingBarClassificacao ;
    private Button buttonSalvar ;
    private String localArquivo;

    private Cachoeira cachoeira ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        imageView  = findViewById(R.id.foto);
        editTextNome = findViewById(R.id.nome);
        editTextInformacoes = findViewById(R.id.informacoes);
        ratingBarClassificacao = findViewById(R.id.rtClassificacao);
        buttonSalvar = findViewById(R.id.btnSalvar);

        Intent intent = getIntent();

        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA);

        if (cachoeira == null){
            cachoeira = new Cachoeira();
        }else {
            preencher(cachoeira);
        }


    }



    private void preencher (Cachoeira cachoeira ){
        editTextNome.setText(cachoeira.getNome());
        editTextInformacoes.setText(cachoeira.getInformocoes());
        ratingBarClassificacao.setRating(cachoeira.getClassificacao());

        Bitmap fotoOrg = BitmapFactory.decodeFile(cachoeira.getImagem());

        Bitmap fotoRdz = Bitmap.createScaledBitmap(fotoOrg , 200,200,true );

        imageView.setImageBitmap(fotoRdz);

    }





    public void capturarImagem(View view){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE) ;

        String caminhoSD = Environment.getExternalStorageDirectory().toString();

        localArquivo = caminhoSD + "/"+System.currentTimeMillis() + ".png";

        File arqv = new File(localArquivo);

        Uri localArmazenado = Uri.fromFile(arqv);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, localArmazenado);

        startActivityForResult(intent, CAPTURA_IMAGEM);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (CAPTURA_IMAGEM == requestCode){

            if(resultCode == RESULT_OK){

                Bitmap fotoOrg = BitmapFactory.decodeFile(localArquivo);

                Bitmap fotoRdz = Bitmap.createScaledBitmap(fotoOrg , 200,200,true );

                imageView.setImageBitmap(fotoRdz);

            }
        }



    }


    public void salvar (View view){

        String nome = editTextNome.getText().toString() ;
        String informacao = editTextInformacoes.getText().toString() ;
        float classificacao = ratingBarClassificacao.getRating() ;
        String imagem = localArquivo;


        cachoeira.setNome(nome);
        cachoeira.setInformocoes(informacao);
        cachoeira.setClassificacao(classificacao);
        cachoeira.setImagem(imagem);

        try {
            dao = new CachoeiraDAO(NovoActivity.this);
            dao.salvar(cachoeira);
            dao.close();
            Toast.makeText(NovoActivity.this , "Salvo com sucesso", Toast.LENGTH_LONG).show();

        }catch (Exception execsao){
            Toast.makeText(NovoActivity.this,"Erro",Toast.LENGTH_LONG).show();
        }



       /* // voltar main activity
        Intent intent = new Intent();
        intent.putExtra(MainActivity.CACHOEIRA , cachoeira) ;
        setResult(RESULT_OK , intent);*/

        finish();



    }






























}
