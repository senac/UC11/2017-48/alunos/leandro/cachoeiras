package br.com.senac.cachoeira.DAO;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.model.Cachoeira;

public class CachoeiraDAO extends SQLiteOpenHelper {

    private static final String DATA = "SQLite";
    private static final int VERSAO = 4;


    public CachoeiraDAO(Context context) {
        super(context, DATA, null, VERSAO);
    }


    @Override
    public void onCreate(SQLiteDatabase dataBase) {

        String ddl = "CREATE TABLE Cachoeira ( id INTEGER PRIMARY KEY ,nome TEXT, informacoes TEXT ,  classificacao REAL ,  imagem TEXT);";

        dataBase.execSQL(ddl);

    }

    @Override
    public void onUpgrade(SQLiteDatabase dataBase, int i, int i1) {

        String ddl  = "DROP TABLE IF EXIST Cachoeira ; ";
        dataBase.execSQL(ddl);
        this.onCreate(dataBase);

    }

    public void salvar (Cachoeira cachoeira){

        ContentValues values = new ContentValues();
        values.put("nome", cachoeira.getNome());
        values.put("informacoes" , cachoeira.getInformocoes());
        values.put("imagem", (cachoeira.getImagem()));
        values.put("classificacao", cachoeira.getClassificacao());

        if(cachoeira.getId()==0){
            getWritableDatabase().insert("Cachoeira", null, values);
        }else{
            getWritableDatabase().update("Cachoeira",values,"id = " + cachoeira.getId(), null);
        }



    }

    public List<Cachoeira> getLista (){

        List<Cachoeira> lista = new ArrayList<>();
        String colunas [] = {"id","nome","informacoes","classificacao","imagem"};

        Cursor cursor = getWritableDatabase().query("Cachoeira", colunas, null,null, null, null, null);

        while (cursor.moveToNext() ){

            Cachoeira cachoeira = new Cachoeira();
            cachoeira.setId(cursor.getInt(0));
            cachoeira.setNome(cursor.getString(1));
            cachoeira.setInformocoes(cursor.getString(2));
            cachoeira.setClassificacao((float) cursor.getDouble(3));
            cachoeira.setImagem(cursor.getString(4));
            lista.add(cachoeira);

        }



   return lista;
    }


}
